package com.ashraful.bookapp.model

data class BookListModel(
    val `data`: List<Book>,
    val message: String,
    val status: Int
) {
    data class Book(
        val id: Int,
        val image: String,
        val name: String,
        val url: String
    )
}