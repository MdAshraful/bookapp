package com.ashraful.bookapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ashraful.bookapp.data.remote.api.BookService
import com.ashraful.bookapp.data.utils.NetworkResult
import com.ashraful.bookapp.mapper.BookDtoMapper.toBookListModel
import com.ashraful.bookapp.model.BookListModel
import javax.inject.Inject

class BookRepository @Inject constructor (private val service: BookService) {

    private val _booksResponseLiveData = MutableLiveData<NetworkResult<BookListModel>>()
    val booksResponseLiveData: LiveData<NetworkResult<BookListModel>>
        get() = _booksResponseLiveData

    suspend fun getBooks() {
        _booksResponseLiveData.postValue(NetworkResult.Loading())
        val response = service.getBooks()

        if (response.isSuccessful && response.body() != null) {
            _booksResponseLiveData.postValue(NetworkResult.Success(response.body()!!.toBookListModel()))
        } else if (response.errorBody() != null) {
            try {
                _booksResponseLiveData.postValue(NetworkResult.Error(response.message()))
            } catch (e: Exception) {
                _booksResponseLiveData.postValue(NetworkResult.Error("Mess: " + e.message + "  Trace: " + e.printStackTrace()))
            }
        } else {
            _booksResponseLiveData.postValue(NetworkResult.Error("Something went wrong!"))
        }
    }


}