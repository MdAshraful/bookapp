package com.ashraful.bookapp.data.remote.api

import com.ashraful.bookapp.data.remote.dto.BookListDto
import com.ashraful.bookapp.data.utils.Constants
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Response
import retrofit2.http.GET

interface BookService {

    @GET(Constants.BOOK_LIST_ENDPOINT)
    suspend fun getBooks(): Response<BookListDto>
}