package com.ashraful.bookapp.data.ui.booklist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.ashraful.bookapp.R
import com.ashraful.bookapp.data.ui.booklist.view.BookListActivity
import com.ashraful.bookapp.model.BookListModel
import com.squareup.picasso.Picasso

class BookListAdapter(private val context: Context, private val books: List<BookListModel.Book>): RecyclerView.Adapter<BookListAdapter.DataViewHolder>() {

    inner class DataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val name: TextView = itemView.findViewById(R.id.tvBookName)
        val image: ImageView = itemView.findViewById(R.id.iv_book)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_book_item, parent, false)
        return DataViewHolder(view)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val item = books[position]

        holder.name.setOnClickListener {
            //context.startActivity()
        }

        holder.name.text = item.id.toString()

        // Load image with Picasso
        Picasso.get().load(item.image).into(holder.image)
    }
}