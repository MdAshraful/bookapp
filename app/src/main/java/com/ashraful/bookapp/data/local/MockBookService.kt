package com.ashraful.bookapp.data.local

import android.content.Context
import com.ashraful.bookapp.data.remote.api.BookService
import com.ashraful.bookapp.data.remote.dto.BookListDto
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Response

class MockBookService(private val context: Context) : BookService {

    override suspend fun getBooks(): Response<BookListDto> {
        // Read the JSON file from assets
        val inputStream = context.assets.open("sample_data.json")
        val jsonString = inputStream.bufferedReader().use { it.readText() }

        // Parse JSON to BookListDto
        val bookListType = object : TypeToken<BookListDto>() {}.type
        val bookListDto = Gson().fromJson<BookListDto>(jsonString, bookListType)

        // Create a success response with the mock data
        return Response.success(bookListDto)
    }
}
