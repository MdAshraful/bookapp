package com.ashraful.bookapp.data.ui.booklist.view

import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import androidx.recyclerview.widget.RecyclerView.Orientation
import com.ashraful.bookapp.R
import com.ashraful.bookapp.data.ui.booklist.adapter.BookListAdapter
import com.ashraful.bookapp.data.ui.booklist.viewmodel.BookListViewModel
import com.ashraful.bookapp.data.utils.NetworkResult
import com.ashraful.bookapp.databinding.ActivityBookListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookListActivity : AppCompatActivity() {

    private val viewmodel by viewModels<BookListViewModel>()

    private var _binding: ActivityBookListBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        _binding = ActivityBookListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }


        viewmodel.getBooks()

        liveDataObserver()
    }

    private fun liveDataObserver(){
        viewmodel.booksLiveData.observeForever(Observer { result ->
            binding.loader.isVisible = false
            when(result){
                is NetworkResult.Error -> {
                    Toast.makeText(this, result.message, Toast.LENGTH_SHORT).show()
                }
                is NetworkResult.Loading -> {
                    binding.loader.isVisible = true
                }
                is NetworkResult.Success -> {
                    if (result.data != null){
                        binding.rvBook.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                        binding.rvBook.adapter = BookListAdapter(this, result.data.data)
                    }
                }
            }
        })
    }
}