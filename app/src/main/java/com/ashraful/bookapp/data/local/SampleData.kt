package com.ashraful.bookapp.data.local

import com.ashraful.bookapp.model.BookListModel

object SampleData {

    val localResponse: BookListModel = BookListModel(
        message = "Success",
        status = 200,
        data = generateBooks()
    )

    private fun generateBooks(): List<BookListModel.Book> {
        return listOf(
            createBook(
                id = 101,
                name = "Programming Language C",
                imageUrl = "https://picsum.photos/id/1/300/300",
                url = "https://en.wikipedia.org/wiki/C_(programming_language)"
            ),
            createBook(
                id = 102,
                name = "The Lord of the Rings",
                imageUrl = "https://picsum.photos/id/2/300/300",
                url = "https://en.wikipedia.org/wiki/The_Lord_of_the_Rings"
            ),
            createBook(
                id = 103,
                name = "Pride and Prejudice",
                imageUrl = "https://picsum.photos/id/3/300/300",
                url = "https://en.wikipedia.org/wiki/Pride_and_Prejudice"
            ),
            createBook(
                id = 104,
                name = "The Hitchhiker's Guide to the Galaxy",
                imageUrl = "https://picsum.photos/id/4/300/300",
                url = "https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy"
            ),
            createBook(
                id = 105,
                name = "The Great Gatsby",
                imageUrl = "https://picsum.photos/id/5/300/300",
                url = "https://en.wikipedia.org/wiki/The_Great_Gatsby"
            ),
            createBook(
                id = 106,
                name = "One Hundred Years of Solitude",
                imageUrl = "https://picsum.photos/id/6/300/300",
                url = "https://en.wikipedia.org/wiki/One_Hundred_Years_of_Solitude"
            ),
            createBook(
                id = 107,
                name = "Moby Dick",
                imageUrl = "https://picsum.photos/id/7/300/300",
                url = "https://en.wikipedia.org/wiki/Moby_Dick"
            ),
            createBook(
                id = 108,
                name = "Hamlet",
                imageUrl = "https://picsum.photos/id/8/300/300",
                url = "https://en.wikipedia.org/wiki/Hamlet"
            ),
            createBook(
                id = 109,
                name = "Crime and Punishment",
                imageUrl = "https://picsum.photos/id/9/300/300",
                url = "https://en.wikipedia.org/wiki/Crime_and_Punishment"
            ),
            createBook(
                id = 110,
                name = "War and Peace",
                imageUrl = "https://picsum.photos/id/10/300/300",
                url = "https://en.wikipedia.org/wiki/War_and_Peace"
            ),
            createBook(
                id = 111,
                name = "Frankenstein",
                imageUrl = "https://picsum.photos/id/19/300/300",
                url = "https://en.wikipedia.org/wiki/Frankenstein"
            ),
            createBook(
                id = 112,
                name = "The Adventures of Sherlock Holmes",
                imageUrl = "https://picsum.photos/id/21/300/300",
                url = "https://en.wikipedia.org/wiki/The_Adventures_of_Sherlock_Holmes"
            ),
            createBook(
                id = 113,
                name = "The Odyssey",
                imageUrl = "https://picsum.photos/id/22/300/300",
                url = "https://en.wikipedia.org/wiki/The_Odyssey"
            ),
            createBook(
                id = 114,
                name = "The Adventures of Huckleberry Finn",
                imageUrl = "https://picsum.photos/id/23/300/300",
                url = "https://en.wikipedia.org/wiki/The_Adventures_of_Huckleberry_Finn"
            ),
            createBook(
                id = 115,
                name = "The Catcher in the Rye",
                imageUrl = "https://picsum.photos/id/24/300/300",
                url = "https://en.wikipedia.org/wiki/The_Catcher_in_the_Rye"
            )
        )
    }

    private fun createBook(
        id: Int,
        name: String,
        imageUrl: String,
        url: String
    ): BookListModel.Book {
        return BookListModel.Book(id = id, name = name, image = imageUrl, url = url)
    }

}