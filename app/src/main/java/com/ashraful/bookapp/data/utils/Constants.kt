package com.ashraful.bookapp.data.utils

object Constants {

    const val BASE_URL = "https://example.com/api/"
    const val BOOK_LIST_ENDPOINT = "book-list"
}