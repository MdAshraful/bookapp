package com.ashraful.bookapp.data.remote.dto

import com.google.gson.annotations.SerializedName

class BookListDto (
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("url")
        val url: String
    )
}