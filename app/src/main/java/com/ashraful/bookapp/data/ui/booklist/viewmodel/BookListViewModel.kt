package com.ashraful.bookapp.data.ui.booklist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.viewModelFactory
import com.ashraful.bookapp.data.utils.NetworkResult
import com.ashraful.bookapp.model.BookListModel
import com.ashraful.bookapp.repository.BookRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookListViewModel @Inject constructor(private val repository: BookRepository): ViewModel() {

    val booksLiveData: LiveData<NetworkResult<BookListModel>>
        get() = repository.booksResponseLiveData

    fun getBooks(){
        viewModelScope.launch {
            repository.getBooks()
        }
    }
}