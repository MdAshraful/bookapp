package com.ashraful.bookapp.mapper

import com.ashraful.bookapp.data.remote.dto.BookListDto
import com.ashraful.bookapp.model.BookListModel

object BookDtoMapper {
    fun BookListDto.toBookListModel(): BookListModel {
        val bookList = data.map { bookData ->
            BookListModel.Book(
                id = bookData.id,
                image = bookData.image,
                name = bookData.name,
                url = bookData.url
            )
        }
        return BookListModel(bookList, message, status)
    }
}